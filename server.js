import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import passport from 'passport';

require('./app/models/slotmachine');
require('./app/models/user');
const slotmachineApi = require('./app/routes/slotmachineApi');
const authApi = require('./app/routes/authApi');
const userApi = require('./app/routes/userApi');
const reportApi = require('./app/routes/reportApi');
const app = express();

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

//Initialize passport
app.use(passport.initialize());

//Configure strategy
require('./config/passport')(passport);

mongoose.connect(process.env.DBURI);

app.use('/',authApi);
app.use('/',userApi);
app.use('/',slotmachineApi);
app.use('/',reportApi);

var port_number = process.env.PORT || 3000;
app.listen(port_number);