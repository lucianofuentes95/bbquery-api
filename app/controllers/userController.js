const mongoose = require('mongoose');
const User = mongoose.model('User');

const getAll = function (req, res) {

    var query = { skip: 0, limit: 30 };

    if (req.query.skip || req.query.limit) {

        const skip = parseInt(req.query.skip);
        const limit = parseInt(req.query.limit);

        if (skip && skip < 0) {
            res.status(505);

        }
        if (limit && limit <= 0 && limit > 30) {
            res.status(505);

        }
        query.skip = skip;
        query.limit = limit;
    }


    User.count({}, (err, totalDocuments) => {
        if (err)
            res.status(505);


        User.find({}, '-password -__v', query, (err, users) => {
            if (err)
                res.status(505);
            else if (!users)
                res.status(404).json({ success: false, message: 'No se encontro el Usuario' });
            else {
                var totalPages = Math.ceil(totalDocuments / query.limit);
                res.status(202).json(users);
            }
        });
    });

}

const get = function (req, res) {

    User.findOne({ '_id': req.params.id }, '-password -__v', (err, user) => {
        if (err)
            res.status(505);
        else if (!user)
            res.status(404).json({ message: 'No se encontro el Usuario' });
        else
            res.status(202).json(user);
    });
}

const getUserByEmail = function (req, res) {

    User.findOne({ 'email': req.params.email }, '-password -__v -_id', (err, user) => {
        if (err)
            res.status(505);
        else if (!user)
            res.status(404).json({ message: 'User not found' });
        else
            res.status(202).json(user);
    });
}

const update = function (req, res) {

    User.findOneAndUpdate({ '_id': req.params.id }, { 'username': req.body.username, 'email': req.body.email, 'role': req.body.role }, (err, user) => {
        if (err)
            res.status(505).json(err);
        else if (!user)
            res.status(404).json({ message: 'No se encontro el Usuario' });
        else
            res.status(202).json({ message: 'Usuario actualizado correctamente', user: user });

    });

}

const remove = function (req, res) {

    User.findOne({ '_id': req.params.id }, (err, user) => {
        if (err)
            res.status(505).json(err);
        else if (!user)
            res.status(404).json({ message: 'No se encontro el Usuario' });
        else if (user)
            user.deleteOne((err) => {
                if (err) {
                    res.status(505).json(err);
                }
                else {
                    res.status(202).json({ message: 'Usuario eliminado correctamente' })
                }
            });
    });
}

const add = function (req, res) {
    if (!req.body.email || !req.body.password || !req.body.role || !req.body.username) {
        res.json({ message: 'Por favor ingrese un email y contraseña para registrarse' });
    } else {
        var newUser = new User({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            role: req.body.role
        });

        newUser.save(function (err) {
            if (err) {
                res.status(409).json({ message: 'El mail ya se encuentra registrado' });
            }
            else {
                var userWithoutPass = newUser;
                delete userWithoutPass.password;
                res.status(202).json({ message: 'Usuario creado exitosamente!', user: userWithoutPass });
            }
        })
    }
}

module.exports = { get, getUserByEmail, getAll, update, remove, add };