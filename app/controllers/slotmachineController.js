const mongoose = require('mongoose');
const Slotmachine = mongoose.model('Slotmachine');
const Report = mongoose.model('Report');
const Comment = mongoose.model('Comment');
const User = mongoose.model('User');
const csv = require('fast-csv');
const fs = require('fs');
const util = require('util');

const getAll = function (req, res) {

    var query = { skip: 0, limit: 30 };
    var select = { _id: 0 }

    if (req.query.page || req.query.pageSize) {

        const skip = parseInt(req.query.page);
        const limit = parseInt(req.query.pageSize);

        if (skip && skip < 0) {
            res.status(505);
        }
        if (limit && limit <= 0 && limit > 30) {
            res.status(505);
        }
        query.skip = skip * limit;
        query.limit = limit;
    }

    Slotmachine.count({}, (err, totalDocuments) => {
        if (err)
            res.status(505);

        Slotmachine.find({}, select, function (err, slotmachines) {
            if (err)
                res.status(505);
            else if (!slotmachines)
                res.status(404).json({ success: false, message: 'No se encontro la Slotmachine' });
            else {
                var totalPages = Math.ceil(totalDocuments / query.limit);
                res.status(202).json({ data: slotmachines, pageSize: query.limit, page: query.skip, length: totalDocuments });
            }
        });
    });
}



const add = function (req, res) {

    Slotmachine.create(req.body, (err, slotmachine) => {
        if (err)
            res.status(505).json(err);
        else if (!slotmachine)
            res.status(404).json({ message: 'No se encontro la Slotmachine' });
        else if (slotmachine)
            res.status(202).json({ data: slotmachine, success: true, message: 'Slotmachine agregada' })
    });
}

const addReport = function (req, res) {

    var query = { UID: req.params.uid }
    var report = new Report(req.body);
    report.state = 1;
    report.author = req.user._id;

    Slotmachine.findOne(query, (err, slotmachine) => {
        if (err)
            res.status(505).json(err);
        else if (!slotmachine)
            res.status(404).json({ message: 'No se encontro la Slotmachine' });
        else if (slotmachine) {

            report.slotmachine = slotmachine._id
            report.save((err, newreport) => {
                if (err)
                    res.status(505).json(err);
                else if (!newreport)
                    res.status(404).json({ message: 'No se encontro la Slotmachine' });
                else if (newreport) {
                    slotmachine.reports.push(newreport._id);
                    slotmachine.save((err) => {
                    })
                    res.status(202).json({ message: 'Reporte agregado', report: newreport })
                }
            });

        }
    });
}

const get = function (req, res) {

    Slotmachine.findOne({ 'UID': req.params.uid }, '-__v -_id', (err, slotmachine) => {
        if (err)
            res.status(505);
        else if (!slotmachine)
            res.status(404).json({ message: 'No se encontro la Slotmachine' });
        else
            res.status(202).json(slotmachine);
    });
}

const getReports = function (req, res) {

    Slotmachine
        .findOne({ 'UID': req.params.uid }, 'reports')
        .populate({
            path: 'reports',
            model: 'Report',
            populate: {
                path: 'author',
                select: 'username',
                model: 'User',
            }
        })
        .exec(function (err, slotmachine) {
            if (err)
                res.status(505);
            else if (!slotmachine)
                res.status(404).json({ success: false, message: 'No se encontro la Slotmachine' });
            else {
                res.status(202).json(slotmachine.reports);
            }
        });
}

const update = function (req, res) {

    delete req.body.UID;
    Slotmachine.findOneAndUpdate({ 'UID': req.params.uid }, { $set: req.body }, { useFindAndModify: false, new: true }, (err, slotmachine) => {
        if (err) {
            res.status(404).json(err);
        }
        else if (!slotmachine)
            res.status(404).json({ message: 'No se encontro la Slotmachine' });
        else if (slotmachine) {
            res.status(202).json({ data: slotmachine, message: 'Slotmachine actualizada' })
        }
    });
}

const remove = function (req, res) {

    Slotmachine.findOne({ 'UID': req.params.uid }, (err, slotmachine) => {
        if (err)
            res.status(505).json(err);
        else if (!slotmachine)
            res.status(404).json({ message: 'No se encontro la Slotmachine' });
        else if (slotmachine)
            slotmachine.deleteOne((err) => {
                if (err) {
                    res.status(505).json(err);
                }
                else {
                    res.status(202).json({ uid: req.params.uid, message: 'Slotmachine eliminada' })
                }
            });
    });
}

const removeAll = function (req, res) {

    Slotmachine.deleteMany({}, function (err) {
        if (err)
            res.json(err);
        else
            res.status(202).json({ success: true, message: 'Eliminadas todas las Slotmachines' })
    });
}


const count = function (req, res) {
    Slotmachine.find().countDocuments(function (err, count) {
        if (err)
            console.log(err);
        else
            res.status(200).json(count);
    });
}

const addList = function (req, res) {

    var fileRows = [];

    if (!req.file) {
        res.status(404).json({ message: "Error: Solo se permiten archivos CSV" });
        return;
    }

    //TODO
    // - Check fields
    csv.fromPath(req.file.path, { headers: true, delimiter: ';', quote: '"' })
        .on("data", function (data) {
            fileRows.push(data);
        })
        .on("end", function () {
            for (var slot of fileRows) {
                if (checkIfKeys(Object.keys(slot))) {
                    Slotmachine.updateOne({ 'UID': slot.UID }, { $set: slot }, { upsert: true }, function (err, rawResponse) {
                        if (err) {
                            return res.status(404).json(err);
                        }
                    });
                }
                else {
                    return res.status(404).json({ message: "CSV Cabeceras erroneas" });
                }
            }
            fs.unlinkSync(req.file.path);
            return res.status(202).json({ message: "CSV Subido exitosamente" });
        });
}

const checkIfKeys = function (data) {
    return data.includes('Isla') && data.includes('Posc')
        && data.includes('Concentrador')
        && data.includes('IPConcentrador')
        && data.includes('IPMaquina')
        && data.includes('MID') && data.includes('UID')
        && data.includes('Marca')
        && data.includes('Modelo')
        && data.includes('Mueble')
        && data.includes('TipodeJuego')
        && data.includes('Serie') && data.includes('Tipo')
        && data.includes('DenoSAS')
        && data.includes('Devolucion')
        && data.includes('Progresivo')
        && data.includes('DesProg')
        && data.includes('ApMin')
        && data.includes('ApMax')
        && data.includes('Maximo')
        && data.includes('TabladePago')
        && data.includes('Programa')
        && data.includes('MesAnioFab') && data.includes('Sector');
}



module.exports = { getReports, add, addReport, removeAll, getAll, get, remove, addList, update, count };