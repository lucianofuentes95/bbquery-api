const mongoose = require('mongoose');
const User = mongoose.model('User');
const jwt = require('jsonwebtoken');
const config = require('../../config/main');

const Register = function (req, res) {
    if (!req.body.email || !req.body.password || !req.body.role || !req.body.username) {
        res.json({ success: false, message: 'Por favor ingres un email y contraseña para registrarse' });
    } else {
        var newUser = new User({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            role: req.body.role
        });

        newUser.save(function (err) {
            if (err) {
                res.status(401).json({ success: false, message: 'El mail ya se encuentra registrado' });
            }
            else {
                res.status(202).json({ success: true, message: 'Usuario creado exitosamente!' });
            }
        })
    }
}

const Authenticate = function (req, res) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.status(404).json({ success: false, message: 'La contraseña o usuario que ha ingresado es incorrecta.' });
        } else {
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    var token = jwt.sign(JSON.parse(JSON.stringify(user)), config.secret, {
                        expiresIn: 10000,
                    });
                    res.json({ success: true, token, email: user.email, role: user.role });
                } else {
                    res.status(404).json({ success: false, message: 'La contraseña o usuario que ha ingresado es incorrecta.' });
                }
            });
        }
    });
}

const ChangePassword = function (req, res) {
    User.findOne({ _id: req.user._id }, (err, user) => {
        if (err) {
            res.status(505)
        }
        if (!user) {
            res.status(404).json({ message: 'No se encontro el Usuario' });
        }
        else if (user) {
            user.comparePassword(req.body.password, (err, isMatch) => {
                if (isMatch && !err) {
                    user.password = req.body.newpassword
                    user.save((err) => {
                        if (err) {
                            res.status(505).json({ success: false, message: 'Error de servidor' });
                        }
                        else {
                            res.status(202).json({ success: true, message: 'Contraseña Actualizada' });
                        }
                    });
                } else {
                    res.status(404).json({ success: false, message: 'La contraseña o usuario que ha ingresado es incorrecta.' });
                }
            });
        }
    });
}

const Authenticated = function (req, res) {
    console.log(req);
}

module.exports = { Authenticate, Register, Authenticated, ChangePassword };