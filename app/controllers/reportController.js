const mongoose = require('mongoose');
const Report = mongoose.model('Report');
const Slotmachine = mongoose.model('Slotmachine');
const Comment = mongoose.model('Comment');

const get = function (req, res) {

    Report.findById(req.params.id).populate('author', 'username')
        .populate({
            path: 'comments',
            model: 'Comment',
            populate: {
                path: 'author',
                select: 'username',
                model: 'User'
            }
        })
        .exec((err, report) => {
            if (err)
                res.status(505);
            else if (!report)
                res.status(404).json({ message: 'No se encontro el Reporte' });
            else
                res.status(202).json(report);
        });

}

const getAll = function (req, res) {

    var query = {};
    var select = { _id: 0 }

    if (req.query.page || req.query.pageSize) {

        const skip = parseInt(req.query.page);
        const limit = parseInt(req.query.pageSize);

        if (skip && skip < 0) {
            res.status(505);
        }
        if (limit && limit <= 0 && limit > 30) {
            res.status(505);
        }
        query.skip = skip * limit;
        query.limit = limit;
    }

    if (req.params.uid) {
        query.UID = req.params.uid
    }

    Report.count({}, (err, totalDocuments) => {
        if (err)
            res.status(505);

        Report.find({})
            .populate({ path: 'author', select: 'username' })
            .populate({ path: 'slotmachine', select: 'UID' })
            .find(query)
            .exec(function (err, reports) {
                if (err)
                    res.status(505);
                else if (!reports)
                    res.status(404).json({ success: false, message: 'No existen reportes' });
                else {
                    var totalPages = Math.ceil(totalDocuments / query.limit);
                    res.status(202).json(reports);
                }
            });
    });
}

const addComment = function (req, res) {
    var query = { _id: req.params.id }

    Report.findOne(query, (err, report) => {
        if (err)
            res.status(505).json(err);
        else if (!report)
            res.status(404).json({ message: 'No se encontro el Reporte' });
        else if (report) {

            var comment = new Comment(req.body);
            comment.author = req.user._id;
            comment.report = report._id
            comment.save((err, newcomment) => {
                if (err)
                    res.status(505).json(err);
                else if (!newcomment)
                    res.status(404).json({ message: 'No se encontro el Reporte' });
                else if (newcomment) {
                    report.comments.push(newcomment._id);
                    report.save((err) => {
                        report.populate({
                            path: 'comments',
                            model: 'Comment',
                            populate: {
                                path: 'author',
                                select: 'username',
                                model: 'User'
                            }
                        })
                            .populate('author', (err, doc) => res.status(202).json({ message: 'Comentario Enviado', report: doc }));
                    })
                    //res.status(202).json(report);
                }
            });

        }
    });
}

const update = function (req, res) {

    Report.findOneAndUpdate({ '_id': req.params.id }, { $set: req.body }, { useFindAndModify: false, new: true }, (err, report) => {
        if (err) {
            res.status(404).json(err);
        }
        else if (!report)
            res.status(404).json({ message: 'No se encontro el Reporte' });
        else if (report) {
            res.status(202).json({ message: 'Reporte actualizado', report: report })
        }
    });
}

const remove = function (req, res) {
    Report.findOneAndRemove({ '_id': req.params.id }, (err, report) => {
        if (err)
            res.status(505).json(err);
        else if (!report)
            res.status(404).json({ message: 'No se encontro el Reporte' });
        else if (report) {
            Slotmachine.update({ '_id': report.slotmachine },
                { $pull: { reports: report._id } }, (err, numberAffected) => {
                    if (err)
                        res.status(505).json(err);
                    else {
                        res.status(202).json({ message: 'Reporte eliminado' });
                    }
                });
        }
    });
}

const removeAll = function (req, res) {

    Report.deleteMany({}, function (err) {
        if (err)
            res.json(err);
        else
            res.status(202).json({ message: 'Eliminadas todas los Reportes' })
    });
}

module.exports = { addComment, getAll, get, update, remove, removeAll }