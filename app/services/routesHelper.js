exports.allowOnly = function(accessLevel, callback) {
    function checkUserRole(req, res) {
        if(!(accessLevel & req.user.role)) {
            res.status(403).json({message: 'No posee los permisos correspondientes'});
            return;
        }
        callback(req, res);
    }
    return checkUserRole;
};