const config = module.exports;

const userRoles = config.userRoles = {
    user: 1,     // ...001
    manager: 2,  // ...010
    admin: 4     // ...100
};

config.accessLevels = {
    user: userRoles.manager | userRoles.user | userRoles.admin,     // ...111
    manager: userRoles.manager | userRoles.admin,                    // ...110
    admin: userRoles.admin                                        // ...100
};