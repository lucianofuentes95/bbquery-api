const mongoose = require('mongoose');

const Comment = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    comment: {
        type: String,
        required: true
    },
    report: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Report'
    }
}, { timestamps: true });

mongoose.model('Comment', Comment);

const Report = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slotmachine:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Slotmachine'
    },
    // 0: Consulta
    // 1: Mejora
    // 2: Error
    type: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true,
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    // 0: Cerrado - Resuelto
    // 1: Abierto, 
    state: {
        type: Number,
        required: true
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }]
}, { timestamps: true });

mongoose.model('Report', Report);

const Slotmachine = new mongoose.Schema({
    Isla: String,
    Posc: String,
    Concentrador: String,
    IPConcentrador: String,
    IPMaquina: String,
    MID: String,
    UID: String,
    Marca: String,
    Modelo: String,
    Mueble: String,
    TipodeJuego: String,
    Serie: String,
    Tipo: String,
    Credito: String,
    DenoSAS: String,
    Devolucion: String,
    Progresivo: String,
    DesProg: String,
    ApMin: String,
    ApMax: String,
    Maximo: String,
    TabladePago: String,
    Programa: String,
    MesAnioFab: String,
    Sector: String,
    reports: [{
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Report'
    }],
}, { timestamps: true });

Slotmachine.index({ UID: 1 });

export default mongoose.model('Slotmachine', Slotmachine);