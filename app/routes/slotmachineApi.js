const express = require('express');
const router = express.Router();
const slotmachineController = require('../controllers/slotmachineController');
const reportController = require('../controllers/reportController');
const multer = require('multer');
const roles = require('../models/roles');
const allowOnly = require('../services/routesHelper').allowOnly;
const passport = require('passport');
const upload = multer({
    dest: 'tmp/csv/',
    fileFilter: function (req, file, callback) {
        if(file.mimetype != 'text/csv'){
            return callback(null,false);
        }
        return callback(null,true);
    }
});

//GET uid slotmachine
router.get('/slotmachines/:uid',  passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.user,slotmachineController.get));

//GET all slotmachines
router.get('/slotmachines/',  passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.user,slotmachineController.getAll));

//DELETE uid slotmachine
router.delete('/slotmachines/:uid',  passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,slotmachineController.remove));

//UPDATE uid slotmachine
router.patch('/slotmachines/:uid',  passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,slotmachineController.update));

//POST slotmachines
router.post('/slotmachines/',  passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,slotmachineController.add));

//GET slotmachines's reports
router.get('/slotmachines/:uid/reports/', passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,slotmachineController.getReports));

//POST new report of slotmachines
router.post('/slotmachines/:uid/reports/create', passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,slotmachineController.addReport));

//DELETE all slotmachines
router.delete('/deleteAll', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.manager,slotmachineController.removeAll));

router.get('/count', slotmachineController.count);
router.post('/slotmachine/addList', passport.authenticate('jwt', { session: false }), upload.single('file'), slotmachineController.addList);


module.exports = router;