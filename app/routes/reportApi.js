const express = require('express');
const router = express.Router();
const reportController = require('../controllers/reportController');
const roles = require('../models/roles');
const allowOnly = require('../services/routesHelper').allowOnly;
const passport = require('passport');

//GET report by id
router.get('/reports/:id', passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.user, reportController.get));

//GET all reports
router.get('/reports/', passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.user, reportController.getAll));

//DELETE all reports
router.delete('/reports/deleteAll', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.manager,reportController.removeAll));

//DELETE uid slotmachine
router.delete('/reports/:id',  passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,reportController.remove));

//UPDATE uid slotmachine
router.patch('/reports/:id',  passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,reportController.update));

//POST slotmachines
router.post('/reports/',  passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.manager,reportController.add));


//POST comment on report by id
router.post('/reports/:id/comments/create', passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.user, reportController.addComment));

module.exports = router;