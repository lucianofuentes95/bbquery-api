const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const passport = require('passport');
const roles = require('../models/roles');
const allowOnly = require('../services/routesHelper').allowOnly;

router.get('/users/', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.manager,userController.getAll));

router.get('/users/:id', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.manager,userController.get));

router.get('/users/email/:email', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.manager,userController.getUserByEmail));

router.patch('/users/:id', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.admin,userController.update));

router.delete('/users/:id', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.admin,userController.remove));

router.post('/users/', passport.authenticate('jwt', { session: false}), allowOnly(roles.accessLevels.admin,userController.add));

module.exports = router;