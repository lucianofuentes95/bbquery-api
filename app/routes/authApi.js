const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const passport = require('passport');
const roles = require('../models/roles');
const allowOnly = require('../services/routesHelper').allowOnly;

router.post('/register', authController.Register);

router.post('/authenticate', authController.Authenticate);

router.post('/authenticated', authController.Authenticated);

router.post('/changepassword', passport.authenticate('jwt', { session: false }), allowOnly(roles.accessLevels.user,authController.ChangePassword));

module.exports = router;